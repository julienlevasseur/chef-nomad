name 'default'
default_source :supermarket
default_source :chef_repo, '..'
cookbook 'nomad', path: '../../..'
run_list 'nomad::default'
