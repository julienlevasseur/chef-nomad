#
# Cookbook:: nomad
# Recipe:: default
#

directory node['nomad']['config_dir'] do
  recursive true
end

directory node['nomad']['data_dir'] do
  recursive true
end

nomad node['nomad']['version'] do
  platform node['platform']
end

pretty_settings = Chef::JSONCompat.to_json(node['nomad']['config'])
unless node['nomad']['config']
  template "#{node['nomad']['config_dir']}/config.json" do
    source 'config.json.erb'
    owner node['nomad']['owner']
    group node['nomad']['group']
    mode '0644'
    variables settings: pretty_settings
  end

  node.override['nomad']['systemctl']['ExecStart'] = "#{node['nomad']['install_dir']}/nomad agent -config=#{node['nomad']['config_dir']}/config.json"
end

if node['platform'] == 'ubuntu' || node['platform'] == 'debian'
  template '/lib/systemd/system/nomad.service' do
    source 'nomad.service.erb'
    owner node['nomad']['owner']
    group node['nomad']['group']
    mode '0644'
    variables ExecStart: node['nomad']['systemctl']['ExecStart']
    notifies :run, 'execute[systemctl daemon-reload]', :immediately
    notifies :restart, 'service[nomad]', :delayed
  end

  execute 'systemctl daemon-reload' do
    command 'systemctl daemon-reload'
    action :nothing
    not_if { Chef::Config[:file_cache_path].include?('kitchen') }
  end
#elsif node['platform'] == 'mac_os_x'
#
#  launchd = node['nomad']['systemctl']['ExecStart'].split(' ')
#
#  template '/System/Library/LaunchDaemons/nomad.plist' do
#    source 'nomad.plist.erb'
#    owner node['nomad']['owner']
#    group node['nomad']['group']
#    mode '0644'
#    variables Program: launchd.shift, ProgramArguments: launchd
#    notifies :restart, 'service[nomad]', :delayed
#  end
end

service 'nomad' do
  action [:enable, :start]
  not_if { Chef::Config[:file_cache_path].include?('kitchen') }
  not_if { node['platform'].include?("mac_os_x") }
end

if Chef::Config[:file_cache_path].include?('kitchen')
  node.save # ~FC075
  file '/tmp/kitchen/nodes/node.json' do
    owner 'root'
    group 'root'
    mode 0755
    content ::File.open("/tmp/kitchen/nodes/#{node.name}.json").read
    action :create
    only_if { ::File.exist?("/tmp/kitchen/nodes/#{node.name}.json") }
  end
end
