name 'nomad'
maintainer 'Julien Levasseur'
maintainer_email 'contact@julienlevasseur.ca'
license 'MIT'
description 'Installs/Configures nomad'
long_description 'Installs/Configures nomad'
version '0.1.0'
chef_version '>= 12.14' if respond_to?(:chef_version)
issues_url 'https://gitlab.com/julienlevasseur/chef-nomad/issues'
source_url 'https://gitlab.com/julienlevasseur/chef-nomad'

depends 'zipfile'

supports 'ubuntu'
supports 'centos'
supports 'debian'
