# nomad

![https://gitlab.com/julienlevasseur/chef-nomad/badges/master/pipeline.svg](https://gitlab.com/julienlevasseur/chef-nomad/badges/master/pipeline.svg)

Nomad cookbook install and configure Hashicorp Nomad.
