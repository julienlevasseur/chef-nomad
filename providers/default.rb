use_inline_resources

def whyrun_supported
  true
end

action :install do
  unless nomad_is_installed
    install_nomad
  else
    if nomad_installed_version < new_resource.version
      converge_by "Updating Nomad #{nomad_installed_version} -> #{new_resource.version}" do
        begin
          cleanup
          install_nomad
          Chef::Log.debug("Updating Nomad version: from #{nomad_installed_version} to #{new_resource.version}")
        rescue
          Chef::Log.error("Failed to update Nomad from #{nomad_installed_version} to #{new_resource.version}")
        end
      end
    end
  end
end

def nomad_is_installed
  return ::File.file?("#{new_resource.install_dir}/nomad")
end

def nomad_installed_version
  require 'open3'
  stdout, stderr, status = Open3.capture3(
    "#{new_resource.install_dir}/nomad -v|head -1|awk '{print $2}'|sed -e 's/v//g'"
  )[0]
  return stdout.sub!("\n", "")
end

def cleanup
  ::File.delete("#{new_resource.install_dir}/nomad")
end

def install_nomad
  converge_by "Installing Nomad '#{new_resource.version}'" do
    begin
      unless Dir.exist?(new_resource.install_dir)
        require 'fileutils'
        FileUtils.mkdir_p "#{new_resource.install_dir}"
      end

      if new_resource.platform == 'ubuntu' || new_resource.platform == 'centos'
        remote_file "/tmp/nomad_#{new_resource.version}_linux_amd64.zip" do
          source "https://releases.hashicorp.com/nomad/#{new_resource.version}/nomad_#{new_resource.version}_linux_amd64.zip"
          mode '0644'
          action :create
        end
      elsif new_resource.platform == 'mac_os_x'
        remote_file "/tmp/nomad_#{new_resource.version}_darwin_amd64.zip" do
          source "https://releases.hashicorp.com/nomad/#{new_resource.version}/nomad_#{new_resource.version}_darwin_amd64.zip"
          mode '0644'
          action :create
        end
      end
      
      if new_resource.platform == 'ubuntu' || new_resource.platform == 'centos'
        zipfile "/tmp/nomad_#{new_resource.version}_linux_amd64.zip" do
          into new_resource.install_dir
        end
      elsif new_resource.platform == 'mac_os_x'
        zipfile "/tmp/nomad_#{new_resource.version}_darwin_amd64.zip" do
          into new_resource.install_dir
        end
      end
      Chef::Log.debug("Installing Nomad version: #{new_resource.version}")
    rescue
      Chef::Log.error("Failed to install Nomad #{new_resource.version}")
    end
  end
end

action :uninstall do
  if nomad_is_installed
    converge_by "Uninstalling Nomad" do
      begin
        cleanup
        Chef::Log.debug("Uninstalling Nomad")
      rescue
        Chef::Log.error("Failed to uninstall Nomad")
      end
    end
  end
end