
default['nomad']['version'] = '0.10.4'

default['nomad']['install_dir'] = '/usr/local/bin'
default['nomad']['config_dir']  = '/etc/nomad'
default['nomad']['data_dir']    = '/var/lib/nomad'

default['nomad']['config'] = {}

default['nomad']['systemctl']['ExecStart'] = "#{node['nomad']['install_dir']}/nomad agent -dev"
default['nomad']['launchd']['ProgramArguments'] = ["#{node['nomad']['install_dir']}/nomad", "agent" "-dev"] # Used for MacOS

if node['platform'] == 'ubuntu'
  default['nomad']['owner'] = 'root'
  default['nomad']['group'] = 'root'
elsif node['platform'] == 'mac_os_x'
  default['nomad']['owner'] = 'root'
  default['nomad']['group'] = 'sys'
end
